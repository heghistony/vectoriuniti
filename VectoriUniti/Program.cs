﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VectoriUniti
{
    class Program
    {
        static void Sortare(int[] v, int n)
        {
            bool contor = false; int aux;
            do
            {
                contor = false;
                for (int i = 0; i < n - 1; i++)
                {
                    if (v[i] > v[i + 1])
                    {
                        aux = v[i];
                        v[i] = v[i + 1];
                        v[i + 1] = aux;
                        contor = true;
                    }
                }
            } while (contor);
        }
        static void Main(string[] args)
        {
            Console.WriteLine("Care este dimensiunea primului vector?");
            int m = int.Parse(Console.ReadLine());
            int[] v1 = new int[m];
            Console.WriteLine("Care este dimensiunea pentru al doilea vector?");
            int n = int.Parse(Console.ReadLine());
            int[] v2 = new int[n];
            Random rnd = new Random();
            int[] v = new int[m + n];

            for (int p = 0; p < m; p++)
            {
                v1[p] = rnd.Next(1, 15);
            }
            for (int p = 0; p < n; p++)
            {
                v2[p] = rnd.Next(1, 15);
            }
            Sortare(v1, m);
            Sortare(v2, n);
            int i = 0;
            int k = 0;
            int j = 0;

            while (i < m && j < n)
            {
                if (v1[i] < v2[j])
                {
                    v[k] = v1[i];
                    k++;
                    i++;
                }
                else
                {
                    v[k] = v2[j];
                    k++;
                    j++;
                }
            }
            if (i <= m)
            {
                for (int x = i; x < m; x++)
                {
                    v[k] = v1[x];
                    k++;
                }
            }
            if (j <= n)
            {
                for (int x = j; x < n; x++)
                {
                    v[k] = v2[x];
                    k++;
                }
            }
            for (int x = 0; x < k; x++)
            {
                Console.Write(v[x] + " ");
            }

            Console.ReadKey();
        }
    }
}